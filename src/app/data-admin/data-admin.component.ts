import { Component, OnInit } from '@angular/core';
import { FileInput } from 'ngx-material-file-input';
import { ImportType, DataAdminService } from './data-admin.service';
import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'sie-data-admin',
  templateUrl: './data-admin.component.html',
  styleUrls: ['./data-admin.component.scss']
})
export class DataAdminComponent implements OnInit {

  importing = false;

  selectedType?: ImportType;
  selectedFile?: FileInput;

  importTypes?: Observable<ImportType[]>;

  constructor(
    private readonly dataService: DataAdminService,
    private readonly snack: MatSnackBar
  ) {}

  ngOnInit() {
    this.importTypes = this.dataService.getImporters();
  }

  upload() {
    if (!this.selectedType || !this.selectedFile) {
      return;
    }

    this.importing = true;

    this.dataService
      .upload(this.selectedType.name, this.selectedFile.files[0])
      .pipe(

      )
      .subscribe(
        () => {
          this.selectedFile = undefined;
          this.selectedType = undefined;
          this.importing = false;
          this.snack.open('L\'importation a été éffectué', 'Fermer', { duration: 5000 });
        },
        e => {
          console.error(e);
          this.importing = false;
          this.snack.open('L\'importation a échoué! Contactez un administrateur pour l\'assistance.', 'Fermer');
        }
      );
  }
}
