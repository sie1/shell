import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { CoreConfig } from '../config/types/core';
import { flatMap, map, catchError, delay, tap, retry } from 'rxjs/operators';
import { UserService } from '../login/user.service';

export interface ImportType {
  name: string;
  label: string;
  description: string;
  example?: string;
  fileTypes?: string;
}

interface ImportStatus {
  importId: string;
  status: 'running' | 'done' | 'failed';
  start: string;
  end?: string;
}

@Injectable({
  providedIn: 'root'
})
export class DataAdminService {

  constructor(
    private readonly config: ConfigService,
    private readonly http: HttpClient,
    private readonly userService: UserService
  ) { }

  getImporters(): Observable<ImportType[]> {
    return this.config
      .getSection<CoreConfig>('core')
      .pipe(
        flatMap(config => this.http.get<ImportType[]>(`${config.shell}/data/importer`, { headers: this.userService.authenticationHeader }))
      );
  }

  upload(type: string, file: File): Observable<boolean | string> {
    const formData = new FormData();
    formData.append('file', file, file.name);
    let backendUrl: string;

    return this.config
      .getSection<CoreConfig>('core')
      .pipe(
        tap(config => backendUrl = config.shell),
        flatMap(config => this.http
          .post<ImportStatus>(
            `${config.shell}/data/importer/${type}/import`,
            formData,
            { headers: this.userService.authenticationHeader }
          )
        ),
        map(status => status.importId),
        flatMap(importId => this.waitForImport(type, importId, backendUrl)),
        tap(result => {
          if (typeof result === 'string') {
            throw new Error(result);
          }
        })
      );
  }

  private waitForImport(type: string, id: string, backendUrl: string) {
    return of(null).pipe(
      delay(2500),
      flatMap(() => this.http.get<ImportStatus>(
        `${backendUrl}/data/importer/${type}/import/${id}`,
        { headers: this.userService.authenticationHeader, withCredentials: true }
      )),
      catchError(err => (console.error(err), 'L\'importation a échoué')),
      map(result => {
        if (typeof result === 'string') {
          return result;
        }

        if (result.status === 'failed') {
          return 'L\'importation a échoué';
        }

        if (result.status === 'done') {
          return true;
        }

        // result.status === 'running'
        throw new Error();
      }),
      retry()
    );
  }
}
