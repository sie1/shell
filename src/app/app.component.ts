import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Subscription } from 'rxjs';
import { UserService } from './login/user.service';
import { Router, ActivationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'sie-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy, OnInit {
  title = 'shell';
  sidenav?: MatSidenav;
  sidenavOpen?: boolean;
  isSmall?: boolean;
  sideMode?: string;
  isAuthenticated = false;
  isHome = false;

  private breakPointSubscription?: Subscription;

  constructor(
    breakpointObserver: BreakpointObserver,
    private readonly router: Router,
    private readonly userService: UserService
  ) {
    this.breakPointSubscription = breakpointObserver
      .observe('(max-width: 1279.99px)')
      .subscribe(match => {
        this.isSmall = match.matches;
        this.sideMode = match.matches ? 'over' : 'side';
        if (!match.matches) {
          this.sidenavOpen = true;
        }
      });

    router.events
      .pipe(
        filter(event => event instanceof ActivationEnd)
      )
      .subscribe(event => {
        const routeConfig = (event as ActivationEnd).snapshot.routeConfig;
        this.isHome = !!routeConfig && routeConfig.path === 'accueil';
      });
  }

  ngOnInit() {
    this.userService.user.subscribe(user => {
      if (user.isAuthenticated) {
        this.isAuthenticated = true;
      } else {
        this.isAuthenticated = false;
        this.router
          .navigate(['/enregistrer'])
          .catch(
            error => console.error('La navigation à la page d\'enregistrement a échoué:', error)
          );
      }
    });
  }

  ngOnDestroy() {
    if (this.breakPointSubscription) {
      this.breakPointSubscription.unsubscribe();
      this.breakPointSubscription = undefined;
    }
  }

  logout() {
    this.userService.logout();
    this.isAuthenticated = false;
  }
}
