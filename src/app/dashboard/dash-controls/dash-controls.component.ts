import { Component, OnInit, Inject } from '@angular/core';
import { MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';
import { DashboardService, DashboardComment } from '../dashboard.service';
import { map } from 'rxjs/operators';
import { Dashboard } from 'src/app/dash-tree/types';
import { Comment } from './comments-display/comments-display.component';

export interface DashControlsData {
  dashboard: Dashboard;
  url: string;
}

@Component({
  selector: 'sie-dash-controls',
  templateUrl: './dash-controls.component.html',
  styleUrls: ['./dash-controls.component.scss']
})
export class DashControlsComponent implements OnInit {

  comments?: DashboardComment[];

  constructor(
    readonly sheetRef: MatBottomSheetRef,
    @Inject(MAT_BOTTOM_SHEET_DATA) readonly data: DashControlsData,
    private readonly dashboardService: DashboardService
  ) { }

  ngOnInit() {
    this.dashboardService
      .getCommentsFor(this.data.dashboard.adapter, `${this.data.dashboard.remoteId}`)
      .pipe(
        map(comments => comments.sort((a, b) => b.timestamp.diff(a.timestamp)))
      )
      .subscribe(comments => this.comments = comments);
  }

  copyUrl() {
    const shadowEl = document.createElement('textarea');
    shadowEl.value = this.data.url;
    shadowEl.setAttribute('readonly', '');
    shadowEl.style.position = 'absolute';
    shadowEl.style.left = '-9999px';
    document.body.appendChild(shadowEl);
    shadowEl.select();
    document.execCommand('copy');
    document.body.removeChild(shadowEl);
  }

  appendComment(comment: Comment) {
    this.dashboardService.appendCommentTo(
      this.data.dashboard.adapter,
      `${this.data.dashboard.remoteId}`,
      {
        ...comment,
        adapter: this.data.dashboard.adapter,
        remoteId: `${this.data.dashboard.remoteId}`
      }
    );
  }
}
