import { Component, OnInit, Input } from '@angular/core';
import { DashTreeService } from 'src/app/dash-tree/dash-tree.service';
import { DashTreeSection } from '../types';

@Component({
  selector: 'sie-add-section',
  templateUrl: './add-section.component.html',
  styleUrls: ['./add-section.component.sass']
})
export class AddSectionComponent implements OnInit {

  @Input()
  parent?: DashTreeSection;

  sectionName?: string;

  constructor(private readonly indicatorService: DashTreeService) { }

  ngOnInit() {
  }

  processInput() {
    if (!this.sectionName) {
      return;
    }

    this.indicatorService.addSection(this.parent && this.parent.name, this.sectionName);

    this.sectionName = '';
  }
}
