import { TestBed } from '@angular/core/testing';

import { DashTreeService } from './dash-tree.service';

describe('IndicatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DashTreeService = TestBed.get(DashTreeService);
    expect(service).toBeTruthy();
  });
});
