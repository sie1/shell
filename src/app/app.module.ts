import { MaterialFileInputModule } from 'ngx-material-file-input';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTreeModule } from '@angular/material/tree';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { A11yModule } from '@angular/cdk/a11y';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EmbedDashboardComponent } from './dashboard/embed-dashboard/embed-dashboard.component';
import { IndicatorTreeComponent } from './dash-tree/indicator-tree.component';
import { ImprintComponent } from './imprint/imprint.component';
import { LoginComponent } from './login/login.component';
import { UserSettingsComponent } from './user-settings/user-settings.component';
import { AddSectionComponent } from './dash-tree/add-section/add-section.component';
import { ConfirmDialogComponent } from './dash-tree/confirm-dialog/confirm-dialog.component';
import { DashControlsComponent } from './dashboard/dash-controls/dash-controls.component';
import { CommentsDisplayComponent } from './dashboard/dash-controls/comments-display/comments-display.component';
import { UserAdminComponent } from './user-admin/user-admin.component';
import { EditUserComponent } from './user-admin/edit-user/edit-user.component';
import { AuthorizeDirective } from './login/authorize.directive';
import { DataAdminComponent } from './data-admin/data-admin.component';
import { JoomlaFixerComponent } from './tasks/joomla-fixer/joomla-fixer.component';
import { MatCheckboxModule } from '@angular/material';

@NgModule({
  declarations: [
    AddSectionComponent,
    AppComponent,
    ConfirmDialogComponent,
    EmbedDashboardComponent,
    HomeComponent,
    ImprintComponent,
    IndicatorTreeComponent,
    LoginComponent,
    DashboardComponent,
    UserSettingsComponent,
    DashControlsComponent,
    CommentsDisplayComponent,
    UserAdminComponent,
    EditUserComponent,
    AuthorizeDirective,
    DataAdminComponent,
    JoomlaFixerComponent
  ],
  entryComponents: [
    ConfirmDialogComponent,
    DashControlsComponent
  ],
  imports: [
    A11yModule,
    AppRoutingModule,
    MatBottomSheetModule,
    BrowserModule,
    BrowserAnimationsModule,
    DragDropModule,
    FlexLayoutModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatSelectModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    MaterialFileInputModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
