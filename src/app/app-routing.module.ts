import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ImprintComponent } from './imprint/imprint.component';
import { LoginComponent } from './login/login.component';
import { UserSettingsComponent } from './user-settings/user-settings.component';
import { UserAdminComponent } from './user-admin/user-admin.component';
import { EditUserComponent } from './user-admin/edit-user/edit-user.component';
import { DataAdminComponent } from './data-admin/data-admin.component';
import { JoomlaFixerComponent } from './tasks/joomla-fixer/joomla-fixer.component';

const routes: Routes = [
  {
    path: 'enregistrer',
    component: LoginComponent
  },
  {
    path: 'accueil',
    component: HomeComponent
  },
  {
    path: 'mentions-legales',
    component: ImprintComponent
  },
  {
    path: 'tableau/:dash',
    component: DashboardComponent
  },
  {
    path: 'mon-compte',
    component: UserSettingsComponent
  },
  {
    path: 'utilisateurs',
    component: UserAdminComponent
  },
  {
    path: 'utilisateurs/:username',
    component: EditUserComponent
  },
  {
    path: 'donnees/televerser',
    component: DataAdminComponent
  },
  {
    path: 'tache/correcture-joomla',
    component: JoomlaFixerComponent
  },
  {
    path: '',
    redirectTo: 'enregistrer',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
