import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JoomlaFixerComponent } from './joomla-fixer.component';

describe('JoomlaFixerComponent', () => {
  let component: JoomlaFixerComponent;
  let fixture: ComponentFixture<JoomlaFixerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoomlaFixerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JoomlaFixerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
