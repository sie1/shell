import { TestBed } from '@angular/core/testing';

import { JoomlaFixerService } from './joomla-fixer.service';

describe('JoomlaFixerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JoomlaFixerService = TestBed.get(JoomlaFixerService);
    expect(service).toBeTruthy();
  });
});
