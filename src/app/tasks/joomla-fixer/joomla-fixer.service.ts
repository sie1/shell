import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from 'src/app/config.service';
import { CoreConfig } from 'src/app/config/types/core';
import { flatMap, map } from 'rxjs/operators';
import { UserService } from 'src/app/login/user.service';

@Injectable({
  providedIn: 'root'
})
export class JoomlaFixerService {

  constructor(
    private readonly config: ConfigService,
    private readonly http: HttpClient,
    private readonly userService: UserService
  ) { }

  getPrefixes(): Observable<string[]> {
    return this.config
      .getSection<CoreConfig>('core')
      .pipe(
        flatMap(
          config => this.http.get<PrefixesResponse>(
            `${config.shell}/tasks/joomla/prefixes`,
            { headers: this.userService.authenticationHeader }
          )
        ),
        map(response => response.prefixes)
      );
  }

  getDomains(forPrefix: string): Observable<string[]> {
    return this.config
      .getSection<CoreConfig>('core')
      .pipe(
        flatMap(
          config => this.http.get<DomainsResponse>(
            `${config.shell}/tasks/joomla/prefixes/${forPrefix}/domains`,
            { headers: this.userService.authenticationHeader }
          )
        ),
        map(response => response.domains)
      );
  }

  replaceDomains(forPrefix: string, domains: string[], replacement: string) {
    return this.config
      .getSection<CoreConfig>('core')
      .pipe(
        flatMap(
          config => this.http.post<number>(
            `${config.shell}/tasks/joomla/prefixes/${forPrefix}/domains`,
            { prefix: forPrefix, from: domains, to: replacement },
            { headers: this.userService.authenticationHeader }
          )
        )
      );
  }
}

interface PrefixesResponse {
  prefixes: string[];
}

interface DomainsResponse {
  domains: string[];
}
