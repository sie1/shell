import { Injectable } from '@angular/core';
import { of as observableOf, Observable, isObservable } from 'rxjs';
import { map, flatMap, tap, publishLast, refCount } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  private readonly sectionCache: Observable<Map<string, unknown>>;

  constructor(private readonly http: HttpClient) {
    this.sectionCache = this.http
      .get<Record<string, unknown>>('/config/main.json')
      .pipe(
        map(raw => Object.keys(raw).reduce((cache, key) => cache.set(key, raw[key]), new Map())),
        publishLast(),
        refCount()
      );
  }

  getSection<T>(name: string): Observable<T> {
    return this.sectionCache
      .pipe(
        flatMap(cache => {
          const cached = cache.get(name) as T | undefined;
          return !cached
            ? this.loadIndividualSection<T>(name, cache)
            : isObservable(cached)
            ? cached as unknown as Observable<T>
            : observableOf(cached);
        })
      );
  }

  private loadIndividualSection<T>(name: string, cache: Map<string, unknown>): Observable<T> {
    return this.http
      .get<T>(`/config/${name}`)
      .pipe(
        tap(section => cache.set(name, section))
      );
  }
}
